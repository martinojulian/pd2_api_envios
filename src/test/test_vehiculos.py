from . import BaseTestClass
from entidades import VehiculoEntidad, HorarioVehiculoEntidad
from app import db
import json
from datetime import date, time, datetime

class TestLibros(BaseTestClass):

    def testRutaCrearVehiculo(self):
        with self.app.app_context():
            vehiculo = {
                "patente": "aaa123",
                "volumen":200,
                "horarios": [{
                    "dia": "Lunes",
                    "horaDesde": "10:00",
                    "horaHasta": "15:30",
                },{
                    "dia": "Miercoles",
                    "horaDesde": "12:00",
                    "horaHasta": "17:00",
                }],
            }     

            res = self.client.post('/vehiculos',data=json.dumps(vehiculo),content_type='application/json')

            assert res.status_code == 201
            data = json.loads(res.data.decode())
            assert data.get("msg") == 'Se creo el Vehiculo aaa123'
            #Verifico que se haya creado correctamente
            vehiculoCargado = VehiculoEntidad.query.get(1)
            assert vehiculoCargado.patente == "aaa123"
            assert vehiculoCargado.volumen == 200
            horarios = vehiculoCargado.horarios
            assert len(horarios) == 2
            assert horarios[0].dia == "Lunes"
            assert horarios[0].horaDesde == time(10,0)
            assert horarios[0].horaHasta == time(15,30)
            assert horarios[1].dia == "Miercoles"
            assert horarios[1].horaDesde == time(12,0)
            assert horarios[1].horaHasta == time(17,0)
    

    def testRutaCrearVehiculoFormatoHoraDesdeMal(self):
        with self.app.app_context():
            vehiculo = {
                "patente": "aaa123",
                "volumen":200,
                "horarios": [{
                    "dia": "Lunes",
                    "horaDesde": "10:00",
                    "horaHasta": "15:30",
                },{
                    "dia": "Miercoles",
                    "horaDesde": "12dsa2:00",
                    "horaHasta": "17:00",
                }],
            }     

            res = self.client.post('/vehiculos',data=json.dumps(vehiculo),content_type='application/json')

            assert res.status_code == 400
            data = json.loads(res.data.decode())
            assert data.get("msg") == "horaDesde debe tener un formato '%H:%M'"
            #Verifico que se haya creado correctamente
            
            vehiculoCargado = VehiculoEntidad.query.get(1)
            assert vehiculoCargado == None

    def testRutaCrearVehiculoFormatoHoraHastaMal(self):
        with self.app.app_context():
            vehiculo = {
                "patente": "aaa123",
                "volumen":200,
                "horarios": [{
                    "dia": "Lunes",
                    "horaDesde": "10:00",
                    "horaHasta": "15:30",
                },{
                    "dia": "Miercoles",
                    "horaDesde": "12:00",
                    "horaHasta": "17:030",
                }],
            }     

            res = self.client.post('/vehiculos',json=vehiculo)

            assert res.status_code == 400
            data = json.loads(res.data.decode())
            assert data.get("msg") == "horaHasta debe tener un formato '%H:%M'"
            #Verifico que se haya creado correctamente
            
            vehiculoCargado = VehiculoEntidad.query.get(1)
            assert vehiculoCargado == None

    def testRutaCrearVehiculoFormatoDiaMal(self):
        with self.app.app_context():
            vehiculo = {
                "patente": "aaa123",
                "volumen":200,
                "horarios": [{
                    "dia": "lunes",
                    "horaDesde": "10:00",
                    "horaHasta": "15:30",
                },{
                    "dia": "Miercoles",
                    "horaDesde": "12:00",
                    "horaHasta": "17:30",
                }],
            }     

            res = self.client.post('/vehiculos',data=json.dumps(vehiculo),content_type='application/json')

            assert res.status_code == 400
            data = json.loads(res.data.decode())
            assert data.get("msg") == 'El dia puede ser: "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado", "Domingo"'
            #Verifico que se haya creado correctamente
            
            vehiculoCargado = VehiculoEntidad.query.get(1)
            assert vehiculoCargado == None


    def testRutaListarVehiculos(self):
        with self.app.app_context():
            horario1 = HorarioVehiculoEntidad({
                "dia": "Lunes",
                "horaDesde": "10:00",
                "horaHasta": "15:30",
            })
            horario2 = HorarioVehiculoEntidad({
                "dia": "Miercoles",
                "horaDesde": "12:00",
                "horaHasta": "17:00"
            })
            vehiculo1 = VehiculoEntidad({
                "patente": "aaa123",
                "volumen":200
            })
            vehiculo1.horarios.append(horario1)
            vehiculo1.horarios.append(horario2)
            db.session.add(horario1)
            db.session.add(horario2)
            db.session.add(vehiculo1)

            horario3 = HorarioVehiculoEntidad({
                "dia": "Jueves",
                "horaDesde": "12:00",
                "horaHasta": "17:00"
            })
            vehiculo2 = VehiculoEntidad({
                "patente": "aaa111",
                "volumen":100
            })
            vehiculo2.horarios.append(horario3)
            db.session.add(horario3)
            db.session.add(vehiculo2)
            db.session.commit()

            res = self.client.get('/vehiculos')

            assert res.status_code == 200
            vehiculos = json.loads(res.data.decode())
            assert len(vehiculos) == 2
            vehiculo1 = vehiculos[0]
            assert vehiculo1['id'] == 1
            assert vehiculo1['patente'] == 'aaa123'
            assert vehiculo1['volumen'] == 200
            horarios = vehiculo1['horarios']
            assert len(horarios) == 2
            assert horarios[0]['dia'] == 'Lunes'
            assert horarios[0]['horaDesde'] == "10:00"
            assert horarios[0]['horaHasta'] == "15:30"
            assert horarios[1]['dia'] == 'Miercoles'
            assert horarios[1]['horaDesde'] == "12:00"
            assert horarios[1]['horaHasta'] == "17:00"

            vehiculo1 = vehiculos[1]
            assert vehiculo1['id'] == 2
            assert vehiculo1['patente'] == 'aaa111'
            assert vehiculo1['volumen'] == 100
            horarios = vehiculo1['horarios']
            assert len(horarios) == 1
            assert horarios[0]['dia'] == 'Jueves'
            assert horarios[0]['horaDesde'] == "12:00"
            assert horarios[0]['horaHasta'] == "17:00"

    def testHorarioDiaVehiculo(self):
        with self.app.app_context():
            horario1 = HorarioVehiculoEntidad({
                "dia": "Lunes",
                "horaDesde": "10:00",
                "horaHasta": "15:30",
            })
            horario2 = HorarioVehiculoEntidad({
                "dia": "Miercoles",
                "horaDesde": "12:00",
                "horaHasta": "17:00"
            })
            vehiculo1 = VehiculoEntidad({
                "patente": "aaa123",
                "volumen":200
            })
            vehiculo1.horarios.append(horario1)
            vehiculo1.horarios.append(horario2)
            db.session.add(horario1)
            db.session.add(horario2)
            db.session.add(vehiculo1)

            fecha = date(2021,11,8)
            horario = vehiculo1.horarioPorDia(fecha)
            assert horario.get("horaDesde") == '10:00'
            assert horario.get("horaHasta") == '15:30'
            