from . import BaseTestClass
from entidades import EnvioEntidad, EnvioOrdenEntidad, PaqueteEntidad, VehiculoEntidad, HorarioVehiculoEntidad
from app import db
import json
from datetime import datetime, timedelta, date
from mock import patch
from servicioLibros import ServicioLibros
from repositorio import VehiculoRepositorio, EnvioRepositorio

class TestEnvio(BaseTestClass):

    def cargarVehiculos(self):
        with self.app.app_context():
            horario1 = HorarioVehiculoEntidad({
                "dia": "Lunes",
                "horaDesde": "10:00",
                "horaHasta": "15:30",
            })
            horario2 = HorarioVehiculoEntidad({
                "dia": "Miercoles",
                "horaDesde": "12:00",
                "horaHasta": "17:00"
            })
            vehiculo1 = VehiculoEntidad({
                "patente": "aaa123",
                "volumen":200
            })
            vehiculo1.horarios.append(horario1)
            vehiculo1.horarios.append(horario2)
            db.session.add(horario1)
            db.session.add(horario2)
            db.session.add(vehiculo1)

            horario3 = HorarioVehiculoEntidad({
                "dia": "Jueves",
                "horaDesde": "12:00",
                "horaHasta": "17:00"
            })
            vehiculo2 = VehiculoEntidad({
                "patente": "aaa111",
                "volumen":100
            })
            vehiculo2.horarios.append(horario3)
            db.session.add(horario3)
            db.session.add(vehiculo2)
            db.session.commit()

    @patch.object(ServicioLibros, 'obtenerVolumenLibro')
    def testRutaGenerarEnvioSinVehiculo(self, mockObtenerVolumenLibro):
        with self.app.app_context():
            mockObtenerVolumenLibro.return_value = 3
            data = {"ordenId": 1, "libros": [{"libroId": 12, "cantidad": 2}]}
            res = self.client.post('/envios',data=json.dumps(data),content_type='application/json')
            assert res.status_code == 409

    @patch.object(EnvioRepositorio, 'buscarEnvio')
    @patch.object(VehiculoRepositorio, 'disponiblesDia')
    @patch.object(ServicioLibros, 'obtenerVolumenLibro')
    def testRutaGenerarEnvio(self, mockObtenerVolumenLibro, mockDisponiblesDia, mockBuscarEnvio):
        with self.app.app_context():
            mockObtenerVolumenLibro.return_value = 3
            mockBuscarEnvio.return_value = None
            self.cargarVehiculos()
            mockDisponiblesDia.return_value = VehiculoEntidad.query.filter_by(id=1).all()

            data = {"ordenId": 1, "libros": [{"libroId": 12, "cantidad": 2}]}

            res = self.client.post('/envios',data=json.dumps(data),content_type='application/json')
            assert res.status_code == 200
            data = json.loads(res.data.decode())
            assert data['msg'] == 'Envio generado'
            assert data['envioId'] == 1

            envio = EnvioEntidad.query.get(1)
            assert envio.vehiculoId == 1
            assert len(envio.paquetes) == 1
            assert envio.paquetes[0].cantCajas4 == 2
            assert envio.paquetes[0].cantCajas40 == 0
            assert envio.paquetes[0].ordenId == 1
            assert len(envio.envioOrdenes) == 1
            assert envio.envioOrdenes[0].envioId == 1
            assert envio.envioOrdenes[0].ordenId == 1
            assert envio.fechaEntrega == datetime.now().date()

            assert mockObtenerVolumenLibro.call_count == 1
            assert mockDisponiblesDia.call_count == 1
            assert mockBuscarEnvio.call_count == 1

    @patch.object(EnvioRepositorio, 'buscarEnvio')
    @patch.object(VehiculoRepositorio, 'disponiblesDia')
    @patch.object(ServicioLibros, 'obtenerVolumenLibro')
    @patch.object(EnvioEntidad, 'tieneEspacioDisponible')
    def testRutaGenerarEnvioConEnvioPrevioMock(self, mockTieneEspacioDisponible,mockObtenerVolumenLibro, mockDisponiblesDia, mockBuscarEnvio):
        with self.app.app_context():
            mockObtenerVolumenLibro.return_value = 3
            self.cargarVehiculos()
            mockDisponiblesDia.return_value = VehiculoEntidad.query.filter_by(id=1).all()
            envio = EnvioEntidad({"ordenId": 1, "vehiculoId":1, "fechaEntrega": datetime.now().date()})
            db.session.add(envio)
            mockBuscarEnvio.return_value = envio
            mockTieneEspacioDisponible.return_value = True
            data = {"ordenId": 2, "libros": [{"libroId": 12, "cantidad": 2}]}

            res = self.client.post('/envios',data=json.dumps(data),content_type='application/json')
            assert res.status_code == 200
            data = json.loads(res.data.decode())
            assert data['msg'] == 'Envio generado'
            assert data['envioId'] == 1

            envio = EnvioEntidad.query.get(1)
            assert envio.vehiculoId == 1
            assert len(envio.paquetes) == 1
            assert envio.paquetes[0].cantCajas4 == 2
            assert envio.paquetes[0].cantCajas40 == 0
            assert len(envio.envioOrdenes) == 1
            assert envio.fechaEntrega == datetime.now().date()

            assert mockObtenerVolumenLibro.call_count == 1
            assert mockDisponiblesDia.call_count == 1
            assert mockBuscarEnvio.call_count == 1


    def testBuscarEnvio(self):
        with self.app.app_context():
            self.cargarVehiculos()
            hoy =  datetime.now()
            maniana = hoy + timedelta(days=1)
            envio1 = EnvioEntidad({ "vehiculoId":1, "fechaEntrega": maniana.date()})
            envio2 = EnvioEntidad({ "vehiculoId":2, "fechaEntrega": hoy.date()})
            db.session.add(envio1)
            db.session.add(envio2)
            db.session.commit()

            envio = EnvioRepositorio().buscarEnvio(hoy.date(),2)

            assert envio == envio2

    def testBuscarEnvioNoHay(self):
        with self.app.app_context():
            self.cargarVehiculos()
            hoy =  datetime.now()
            maniana = hoy + timedelta(days=1)
            envio1 = EnvioEntidad({ "vehiculoId":1, "fechaEntrega": maniana.date()})
            envio2 = EnvioEntidad({ "vehiculoId":2, "fechaEntrega": hoy.date()})
            db.session.add(envio1)
            db.session.add(envio2)
            db.session.commit()

            envio = EnvioRepositorio().buscarEnvio(hoy.date(),1)

            assert envio == None

    def testVolumenOcupado(self):
        with self.app.app_context():
            paquete1 = PaqueteEntidad({"cantCajas4": 3, "cantCajas40":1})
            paquete2 = PaqueteEntidad({"cantCajas4": 5, "cantCajas40":0})
            envio1 = EnvioEntidad({ })
            envio1.paquetes.append(paquete1)
            envio1.paquetes.append(paquete2)
            db.session.add(paquete1)
            db.session.add(paquete2)
            db.session.add(envio1)
            db.session.commit()

            volumenOcupado = envio1.volumenOcupado()

            assert volumenOcupado == 72

    @patch.object(EnvioEntidad, 'volumenOcupado')
    def testTieneEspacio(self, mockVolumenOcupado):
        with self.app.app_context():
            mockVolumenOcupado.return_value = 40

            vehiculo = VehiculoEntidad({"patente": "aaa123","volumen":200})
            envio1 = EnvioEntidad({"vehiculoId": 1})
            db.session.add(vehiculo)
            db.session.add(envio1)
            db.session.commit()

            tieneEspacio = envio1.tieneEspacioDisponible(50)

            assert tieneEspacio == True

    @patch.object(EnvioEntidad, 'volumenOcupado')
    def testNoTieneEspacio(self, mockVolumenOcupado):
        with self.app.app_context():
            mockVolumenOcupado.return_value = 160

            vehiculo = VehiculoEntidad({"patente": "aaa123","volumen":200})
            envio1 = EnvioEntidad({"vehiculoId": 1})
            db.session.add(vehiculo)
            db.session.add(envio1)
            db.session.commit()

            tieneEspacio = envio1.tieneEspacioDisponible(50)

            assert tieneEspacio == False

    def testRutaObtenerEnvio(self):
        with self.app.app_context():
            self.cargarVehiculos()
            fecha =  date(2021,11,8)
            
            orden1 = EnvioOrdenEntidad({"ordenId": 2})
            orden2 = EnvioOrdenEntidad({"ordenId": 3})
            envio1 = EnvioEntidad({ "vehiculoId":1, "fechaEntrega": fecha})
            envio2 = EnvioEntidad({ "vehiculoId":2, "fechaEntrega": fecha})
            envio1.envioOrdenes.append(orden1)
            envio1.envioOrdenes.append(orden2)
            db.session.add(orden1)
            db.session.add(orden2)
            db.session.add(envio1)
            db.session.add(envio2)
            db.session.commit()

            res = self.client.get('/envio/1')

            assert res.status_code == 200
            envio = json.loads(res.data.decode())
            assert envio.get("id") == 1
            assert envio.get("vehiculoId") == 1
            assert len(envio.get("ordenes")) == 2
            assert envio.get("ordenes")[0].get("ordenId") == 2
            assert envio.get("ordenes")[1].get("ordenId") == 3
            assert envio.get("fechaEntrega") == "08/11/2021"
            assert envio.get("horarioEntrega").get("horaDesde") == "10:00"
            assert envio.get("horarioEntrega").get("horaHasta") == "15:30"

    def testRutaListarEnvio(self):
        with self.app.app_context():
            self.cargarVehiculos()
            fecha =  date(2021,11,8)
            
            orden1 = EnvioOrdenEntidad({"ordenId": 2})
            orden2 = EnvioOrdenEntidad({"ordenId": 3})
            envio1 = EnvioEntidad({ "vehiculoId":1, "fechaEntrega": fecha})
            fecha2 =  date(2021,11,11)
            orden3 = EnvioOrdenEntidad({"ordenId": 4})
            envio2 = EnvioEntidad({ "vehiculoId":2, "fechaEntrega": fecha2})
            envio1.envioOrdenes.append(orden1)
            envio1.envioOrdenes.append(orden2)
            envio2.envioOrdenes.append(orden3)
            db.session.add(orden1)
            db.session.add(orden2)
            db.session.add(orden3)
            db.session.add(envio1)
            db.session.add(envio2)
            db.session.commit()

            res = self.client.get('/envios')

            assert res.status_code == 200
            envios = json.loads(res.data.decode())
            assert len(envios) == 2

            envio = envios[0]
            assert envio.get("id") == 1
            assert envio.get("vehiculoId") == 1
            assert len(envio.get("ordenes")) == 2
            assert envio.get("ordenes")[0].get("ordenId") == 2
            assert envio.get("ordenes")[1].get("ordenId") == 3
            assert envio.get("fechaEntrega") == "08/11/2021"
            assert envio.get("horarioEntrega").get("horaDesde") == "10:00"
            assert envio.get("horarioEntrega").get("horaHasta") == "15:30"

            envio = envios[1]
            assert envio.get("id") == 2
            assert envio.get("vehiculoId") == 2
            assert len(envio.get("ordenes")) == 1
            assert envio.get("ordenes")[0].get("ordenId") == 4
            assert envio.get("fechaEntrega") == "11/11/2021"
            assert envio.get("horarioEntrega").get("horaDesde") == "12:00"
            assert envio.get("horarioEntrega").get("horaHasta") == "17:00"