import unittest
from paquetes import organizarEnCajas
from . import BaseTestClass
from entidades import PaqueteEntidad
import json
from app import db

class TestPaquetes(unittest.TestCase):

    def testArmarPaquetes1de4Y0De40(self):
        listaDeVolumenes = [4,3,1,4,3]
       
        cajas = organizarEnCajas(listaDeVolumenes)
        
        assert cajas[4] == 4
        assert cajas[40] == 0

    def testArmarPaquetes0de4Y2De40(self):
        listaDeVolumenes = [4,4,1,4,3,2,2,4,4,4,3,1,4,4,4,1,4,3,2,2,4,4,4,3,1,4]
       
        cajas = organizarEnCajas(listaDeVolumenes)
        
        assert cajas[4] == 0
        assert cajas[40] == 2

    def testArmarPaquetes5de4Y1De40(self):
        listaDeVolumenes = [4,4,1,4,3,2,2,4,4,4,3,1,4,4,4,1,4,3,2]
       
        cajas = organizarEnCajas(listaDeVolumenes)
        
        assert cajas[4] == 5
        assert cajas[40] == 1

class TestPaquetesConBase(BaseTestClass):

    def testRutaObtener(self):
        with self.app.app_context():
            paquete = PaqueteEntidad({"cantCajas4": 3, "cantCajas40":2, "ordenId":1})
            db.session.add(paquete)
            db.session.commit()
            res = self.client.get("/paquete/1")

            assert res.status_code == 200
            paquete = json.loads(res.data.decode())

            paquete.get('cantCajas4') == 3
            paquete.get('cantCajas40') == 2
            paquete.get('ordenId') == 1