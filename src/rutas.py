from flask import request,Blueprint, jsonify
from repositorio import *
from excepciones import *

ruta = Blueprint("rutas",__name__,url_prefix="")

@ruta.route('/vehiculos', methods=['POST'])
def crearVehiculo():
    try:
        vehiculo = request.get_json()
        return jsonify({"msg": VehiculoRepositorio().crear(vehiculo)}), 201
    except (FormatoHoraException, EnumException) as e:
        db.session.close()
        return jsonify({"msg": str(e)}), 400

@ruta.route('/vehiculos')
def listarVehiculos():
    return jsonify(VehiculoRepositorio().listar()), 200

@ruta.route('/envios', methods=['POST'])
def generarEnvio():
    try:
        orden = request.get_json()
        return jsonify(EnvioRepositorio().generarEnvio(orden)), 200
    except (NotVehiculoException, NoExisteLibroException) as e:
        return jsonify({"msg": str(e)}), 409
    except Exception:
        db.session.close()

@ruta.route('/envio/<id>')
def obtenerEnvio(id):
    try:
        return jsonify(EnvioRepositorio().obtenerPorId(id)), 200
    except RegistroNoEncontradoException as e:
        return jsonify({"msg": str(e)}), 404

@ruta.route('/envios')
def listarEnvio():
    return jsonify(EnvioRepositorio().listar()), 200

@ruta.route('/paquete/<ordenId>')
def obtenerPaquete(ordenId):
    try:
        return jsonify(PaqueteRepositorio().obtenerPorOrdenId(ordenId)), 200
    except RegistroNoEncontradoException as e:
        return jsonify({"msg": str(e)}), 404