from flask.json import jsonify
from app import db    
from entidades import *
from paquetes import armarPaquetes
from excepciones import NotVehiculoException, RegistroNoEncontradoException
from datetime import datetime, timedelta

class HorarioRepositorio:
    def crear(self, horario):
        horario = HorarioVehiculoEntidad(horario)
        db.session.add(horario)
        return horario
        
class VehiculoRepositorio:
    def crear(self, VehiculoJson):
        Vehiculo = VehiculoEntidad(VehiculoJson)
        for horarioJson in VehiculoJson.get("horarios"):
            horario = HorarioRepositorio().crear(horarioJson)
            Vehiculo.horarios.append(horario)
        db.session.add(Vehiculo)
        db.session.commit()
        return f'Se creo el Vehiculo {Vehiculo.patente}'

    def listar(self):
        vehiculos = VehiculoEntidad.query.all()
        return [vehiculo.json() for vehiculo in vehiculos]

    def disponiblesDia(self, dia):
        diaSemana = dia.weekday()
        diaSemanaStr = HorarioVehiculoEntidad.diaSemana(diaSemanaNumero=diaSemana)
        vehiculos = VehiculoEntidad.query.join(VehiculoEntidad.horarios).filter(HorarioVehiculoEntidad.dia==diaSemanaStr).all()
        return vehiculos

    @staticmethod
    def hayVehiculo(volumenEnvio):
        vehiculo = VehiculoEntidad.query.filter(VehiculoEntidad.volumen>=volumenEnvio).first()
        if not vehiculo:
            #Si no se cargo vehiculo no se puede realizar envio
            raise NotVehiculoException("No hay vehiculos")

class PaqueteRepositorio:

    def obtenerPorOrdenId(self, ordenId):
        paquete = PaqueteEntidad.query.filter_by(ordenId=ordenId).first()
        return paquete.json()
    
class EnvioRepositorio:

    def obtenerPorId(self, id):
        envio = EnvioEntidad.query.get(id)
        if envio:
            return envio.json()
        else:
            raise RegistroNoEncontradoException(f"La envio con el id: {id}, no existe")

    def listar(self):
        envios = EnvioEntidad.query.all()
        return [envio.json() for envio in envios]

    #orden = {"ordenId": 1, "libros": [{"libroId": 12, "cantidad": 2}]}
    def generarEnvio(self, orden):
        paquete = armarPaquetes(orden.get("libros"))
        ordenId = orden.get("ordenId")
        paquete = PaqueteEntidad({"cantCajas4": paquete.get(4) , "cantCajas40": paquete.get(40), "ordenId": ordenId})
        #buscar envio mas cercano
        envio = self.envioDisponible(paquete)
        #Agrego orden al envio
        ordenEnvio = EnvioOrdenEntidad({"ordenId": ordenId})
        envio.paquetes.append(paquete)
        envio.envioOrdenes.append(ordenEnvio)
        db.session.add(paquete)
        db.session.add(ordenEnvio)
        db.session.add(envio)
        db.session.commit()
        return {"msg": "Envio generado", "envioId": envio.id}        

    def envioDisponible(self, paquete):
        volumenEnvio = paquete.volumenPaquete()
        VehiculoRepositorio.hayVehiculo(volumenEnvio)
        
        hoy = datetime.now()
        dia = hoy
        envio = None
        # recorro los dias apartir de hoy para encontrar el envio mas pronto posible
        while not envio:
            vehiculosDisponibles = VehiculoRepositorio().disponiblesDia(dia)
            for vehiculo in vehiculosDisponibles:
                envioPosible = self.buscarEnvio(dia, vehiculo.id)
                if envioPosible:
                    # si ya existe un envio para ese vehiculo y dia, verifico si puede tiene espacio para el nuevo paquete
                    if envioPosible.tieneEspacioDisponible(volumenEnvio):
                        envio = envioPosible
                elif vehiculo.volumen >= volumenEnvio:
                    #si no hay envio para ese vehiculo y dia, y vehiculo tiene capacidad para el paquete, creo envio
                    envio = EnvioEntidad({"fechaEntrega": dia, "vehiculoId": vehiculo.id})            
            dia = dia + timedelta(days=1)     
        return envio


    def buscarEnvio(self, dia, vehiculoId):
        return EnvioEntidad.query.filter_by(fechaEntrega=dia, vehiculoId=vehiculoId).first()

