class FormatoHoraException(Exception):
    pass

class EnumException(Exception):
    pass

class NotVehiculoException(Exception):
    pass

class NoExisteLibroException(Exception):
    pass

class ServicioLibrosException(Exception):
    pass

class RegistroNoEncontradoException(Exception):
    pass