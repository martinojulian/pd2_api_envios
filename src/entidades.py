from app import db
from sqlalchemy.orm import relationship
from sqlalchemy import Enum
from datetime import datetime
from excepciones import FormatoHoraException, EnumException

class VehiculoEntidad(db.Model):
    __tablename__ = "vehiculo" 
    id = db.Column(db.Integer,primary_key=True)
    patente = db.Column(db.String(10))
    volumen = db.Column(db.Integer)
    horarios = relationship("HorarioVehiculoEntidad", back_populates="vehiculo")
    envios = relationship("EnvioEntidad", back_populates="vehiculo")

    def __init__(self, autor):
        self.patente = autor.get('patente')
        self.volumen = autor.get('volumen')

    def json(self):
        return {
            'id': self.id,
            'patente': self.patente,
            'volumen': self.volumen,
            'horarios': [horario.json() for horario in self.horarios]   
        }

    def horarioPorDia(self, dia):
        diaStr = HorarioVehiculoEntidad.diaSemana(dia.weekday())
        horario = next(filter(lambda horario: horario.dia == diaStr, self.horarios))
        horarioJson = horario.json()
        return {"horaDesde": horarioJson.get("horaDesde"), "horaHasta":  horarioJson.get("horaHasta")}


class HorarioVehiculoEntidad(db.Model):
    __tablename__ = "horarioVehiculo" 
    id = db.Column(db.Integer,primary_key=True)
    dia = db.Column(db.String(10))
    vehiculoId = db.Column(db.Integer, db.ForeignKey("vehiculo.id"))
    vehiculo = relationship("VehiculoEntidad", back_populates="horarios")
    horaDesde = db.Column(db.Time)
    horaHasta = db.Column(db.Time)

    formatoHora = '%H:%M'
    listaDiaPosibles = ["Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado", "Domingo"]
    

    def __init__(self, horario):
        if horario.get('dia') in self.listaDiaPosibles:
            self.dia = horario.get('dia')  
        else:
            raise EnumException('El dia puede ser: "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado", "Domingo"')
        self.vehiculoId = horario.get('vehiculoId')
        try:
            self.horaDesde = datetime.strptime(horario.get('horaDesde'),self.formatoHora).time()  
        except ValueError:
            raise FormatoHoraException("horaDesde debe tener un formato '%H:%M'")
        try:
            self.horaHasta = datetime.strptime(horario.get('horaHasta'),self.formatoHora).time()
        except ValueError:
            raise FormatoHoraException("horaHasta debe tener un formato '%H:%M'")
        
    def json(self):
        return {
            'dia': self.dia,          
            'horaDesde': self.horaDesde.strftime(self.formatoHora),          
            'horaHasta': self.horaHasta.strftime(self.formatoHora),          
        }

    @staticmethod
    def diaSemana( diaSemanaNumero):
        dicDiasSemana = {0: "Lunes", 1: "Martes", 2: "Miercoles", 3: "Jueves", 4: "Viernes", 5: "Sabado", 6: "Domingo"}
        return dicDiasSemana[diaSemanaNumero]

class PaqueteEntidad(db.Model):
    __tablename__ = "paquete" 
    id = db.Column(db.Integer,primary_key=True)
    cantCajas4 = db.Column(db.Integer)
    cantCajas40 = db.Column(db.Integer)

    envioId = db.Column(db.Integer, db.ForeignKey("envio.id"))
    envio = relationship("EnvioEntidad", back_populates="paquetes")

    ordenId = db.Column(db.Integer)

    def __init__(self, autor):
        self.cantCajas4 = autor.get('cantCajas4')
        self.cantCajas40 = autor.get('cantCajas40')
        self.ordenId = autor.get('ordenId')

    def json(self):
        return {
            'id': self.id,
            'cantCajas4': self.cantCajas4,
            'cantCajas40': self.cantCajas40,
            'volumenTotal': self.volumenPaquete()
        }

    def volumenPaquete(self):
        return (self.cantCajas4 * 4) + (self.cantCajas40 *40)

class EnvioOrdenEntidad(db.Model):
    __tablename__ = "envioOrden" 
    id = db.Column(db.Integer,primary_key=True)
    envioId = db.Column(db.Integer, db.ForeignKey("envio.id"))
    envio = relationship("EnvioEntidad", back_populates="envioOrdenes")
    ordenId = db.Column(db.Integer)

    def __init__(self, envioOrden):
        self.envioId = envioOrden.get('envioId')
        self.ordenId = envioOrden.get('ordenId')

    def json(self):
        return{
            'ordenId': self.ordenId
        }

class EnvioEntidad(db.Model):
    __tablename__ = "envio" 
    id = db.Column(db.Integer,primary_key=True)
    vehiculoId = db.Column(db.Integer, db.ForeignKey("vehiculo.id"))
    vehiculo = relationship("VehiculoEntidad", back_populates="envios")
    
    paquetes = relationship("PaqueteEntidad", back_populates="envio")
    envioOrdenes = relationship("EnvioOrdenEntidad", back_populates="envio")

    fechaEntrega = db.Column(db.Date)
   
    def __init__(self, envio):
        self.vehiculoId = envio.get('vehiculoId')
        self.fechaEntrega = envio.get('fechaEntrega')

    def json(self):
        return {
            'id': self.id,
            'vehiculoId': self.vehiculoId,
            'ordenes': [orden.json() for orden in self.envioOrdenes], 
            'fechaEntrega': self.fechaEntrega.strftime('%d/%m/%Y'),
            'horarioEntrega': self.vehiculo.horarioPorDia(self.fechaEntrega)
        }

    def volumenOcupado(self):
        return sum(map(lambda x: x.volumenPaquete() , self.paquetes))

    def tieneEspacioDisponible(self, volPaquete):
        volOcupado = self.volumenOcupado() 
        return self.vehiculo.volumen >= (volPaquete + volOcupado)