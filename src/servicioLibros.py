import requests
from excepciones import NoExisteLibroException, ServicioLibrosException
from flask import current_app

class ServicioLibros:

    @staticmethod
    def obtenerVolumenLibro(libroId):
        resp = requests.get(f'{current_app.config["URL_LIBROS"]}libro/{libroId}')
        codResp = resp.status_code
        if codResp == 200:
            libro = resp.json()
            return libro.get("volumen")
        elif codResp == 404:
            raise NoExisteLibroException(f"No existe el libro {libroId}")
        else:
            raise ServicioLibrosException("Problemas con el servicio de libros")