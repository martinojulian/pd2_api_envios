import binpacking
from servicioLibros import ServicioLibros

#Recibe lista de los volumenes de libros 
#Devuelve dic con la cantidad de cajas de volumen 4 y 40 
def organizarEnCajas(listaVolumenes = []):
    dicCantCajas = {4: 0, 40: 0}
    cajasVol40 = binpacking.to_constant_volume(listaVolumenes,40)#devuelve lista de lista, cada lista interna reprensenta una caja con los vol de los libros
    for cajaVol40 in cajasVol40:
        cajas4 = binpacking.to_constant_volume(cajaVol40,4)#Distribuyo la caja de 40 en cajas de 4
        cantCajaVol4 = len(cajas4)
        if cantCajaVol4 < 10:
            #Si divido la caja de 40 en cajas de 4 y obtengo menos de 10 cajas de 4, conviene guardar en cajas de 4 y no de 40
            dicCantCajas[4] += cantCajaVol4
        else:
            dicCantCajas[40] += 1
    return dicCantCajas

def armarPaquetes(libros):
    listaVolumenesLibro = []
    for libro in libros:
        volLibro = ServicioLibros.obtenerVolumenLibro(libro.get("libroId"))
        listaVolumenesLibro += [volLibro] * libro.get("cantidad")
    return organizarEnCajas(listaVolumenesLibro)
    