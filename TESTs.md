<h2>Tests</h2>

<h3>Tests de ruta</h3>

<p>Se realizaron los siguientes test de ruta</p>

<ul>
    <li>
        testRutaGenerarEnvioSinVehiculo
    </li>
    <li>
        testRutaGenerarEnvio
    </li>
    <li>
        testRutaGenerarEnvioConEnvioPrevioMock
    </li>
    <li>
        testRutaObtenerEnvio
    </li>
    <li>
        testRutaListarEnvio
    </li>
    <li>
        testRutaObtener
    </li>
    <li>
        testRutaCrearVehiculo
    </li>
    <li>
        testRutaCrearVehiculoFormatoHoraDesdeMal
    </li>
    <li>
        testRutaCrearVehiculoFormatoHoraHastaMal
    </li>
    <li>
        testRutaCrearVehiculoFormatoDiaMal
    </li>
    <li>
        testRutaListarVehiculos
    </li>
    <li>
        testHorarioDiaVehiculo
    </li>
</ul>

<h3>Tests Unitarios</h3>

<p>Se realizaron los siguientes test de ruta</p>

<ul>
    <li>
        testBuscarEnvio
    </li>
    <li>
        testBuscarEnvioNoHay
    </li>
    <li>
        testVolumenOcupado
    </li>
    <li>
        testTieneEspacio
    </li>
    <li>
        testNoTieneEspacio
    </li>
    <li>
        testArmarPaquetes1de4Y0De40
    </li>
    <li>
        testArmarPaquetes0de4Y2De40
    </li>
    <li>
        testArmarPaquetes5de4Y1De40
    </li>
</ul>