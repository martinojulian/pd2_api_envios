<h1>Api envios libros</h1>

<h3> Entidades </h3>

<ul>
    <li>
        Vehiculo
        <ol>
            <li>id</li>
            <li>patente</li>
            <li>volumen</li>
        </ol>
    </li>
    <li>
        HorarioVehiculo
        <ol>
            <li>id</li>
            <li>dia</li>
            <li>vehiculoId</li>
            <li>horaDesde</li>
            <li>horaHasta</li>
        </ol>
    </li>
    <li>
        Paquete
        <ol>
            <li>id</li>
            <li>cantCajas4</li>
            <li>cantCajas40</li>
            <li>envioId</li>
            <li>ordenId</li>
        </ol>
    </li>
    <li>
        EnvioOrden
        <ol>
            <li>id</li>
            <li>envioId</li>
            <li>ordenId</li>
        </ol>
    </li>
    <li>
        Envio
        <ol>
            <li>id</li>
            <li>vehiculoId</li>
            <li>fechaEntrega</li>
        </ol>
    </li>
</ul>

<h3> Rutas </h3>

<ul>
    <li>
        Vehiculo
        <ol>
            <li>GET /vehiculos</li>
            <li>POST /vehiculos</li>
        </ol>
    </li>
    <li>
        Envio
        <ol>
            <li>POST /envios (Solo se tiene que poder acceder desde el servicio de ordenes)</li>
            <li>GET /envios</li>
            <li>GET /envio/&lt;int:id&gt;</li>
        </ol>
    </li>
    <li>
        Paquete
        <ol>
            <li>GET /paquete/&lt;int:ordenId&gt;</li>
        </ol>
    </li>
</ul>
